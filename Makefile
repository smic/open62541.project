
all:	help
.PHONY:	all

help:
	@echo "Usage: make -C .project [rule...]"
	@echo
	@echo "Rules:"
	@echo "  app			Build open62541 application"
	@echo "  open62541		Build open62541 library"
	@echo "  client_certs		Generate certificate for open62541 client"
	@echo
	@echo "  clean_app		Remove open62541 application"
	@echo "  clean_open62541	Remove open62541 library"
	@echo "  clean_certs		Remove certificate files"

#
# Application
#

APPNAME = $(shell basename `dirname $(CURDIR)`)
SRCS = $(shell find ../src -name "*.c")
OBJS = $(patsubst ../src/%.c,../out/.obj/%.c.obj,$(SRCS))

app:	../out/$(APPNAME)
.PHONY:	app

../out/$(APPNAME):		open62541 $(OBJS)
	@bin/cecho "[$(APPNAME)] Link C objects to $@ ..."
	gcc -o ../out/$(APPNAME) \
		$(OBJS) \
		-Llib -static -lopen62541 -lmbedtls -lmbedcrypto -lmbedx509 -lws2_32

../out/.obj/%.c.obj: 	../src/%.c
	@test -d $(shell dirname $@) || mkdir -p $(shell dirname $@)
	@bin/cecho "[$(APPNAME)] Compile C object $@ from $< ..."
	gcc -c -std=c99 -Iinclude -I../include -o $@ $<

clean_app:
	rm -rf ../out

#
# open62541 libraries
#

open62541:				lib/libmbedtls.a lib/libopen62541.a
.PHONY:	open62541

lib/libopen62541.a:		lib/libmbedtls.a src/open62541
	@bin/cecho "[$(APPNAME)] Configure open62541 library ..."
	cmake -G "MSYS Makefiles" -S src/open62541 -B cmake/open62541 \
		-DMBEDTLS_INCLUDE_DIRS="include" -DMBEDTLS_LIBRARY="lib" \
		-DUA_ENABLE_ENCRYPTION=ON \
		-DCMAKE_INSTALL_PREFIX=$(CURDIR)
	@bin/cecho "[$(APPNAME)] Build and install open62541 library ..."
	make -C cmake/open62541 install

src/open62541:
	@bin/cecho "[$(APPNAME)] Clone open62541 source from Github ..."
	git clone -b v1.0 git@github.com:open62541/open62541.git src/open62541

lib/libmbedtls.a:		src/mbedtls
	@bin/cecho "[$(APPNAME)] Configure mbedtls library ..."
	cmake -G "MSYS Makefiles" -S src/mbedtls -B cmake/mbedtls \
		-DENABLE_PROGRAMS=OFF -DENABLE_TESTING=OFF \
		-DCMAKE_INSTALL_PREFIX=$(CURDIR)
	@bin/cecho "[$(APPNAME)] Build and install mbedtls library ..."
	make -C cmake/mbedtls install

src/mbedtls:
	@bin/cecho "[$(APPNAME)] Clone mbedtls source from Github ..."
	git clone -b mbedtls-2.16 git@github.com:ARMmbed/mbedtls.git src/mbedtls

clean_open62541:
	rm -rf cmake include lib share src
.PHONY:	clean_open62541

#
# Certificate
#

OPEN62541_CLIENT_URI = "urn:unconfigured:application"

client_certs:
	@bin/cecho "[$(APPNAME)] Create Self-Signed Certificate for Client ..."
	UA_APPLICATION_URI=$(OPEN62541_CLIENT_URI) openssl req \
		-new -nodes -x509 -sha256 -config certs/client.cnf \
		-keyout /tmp/client_key.pem -out /tmp/client_cert.pem
	@test -d ../out || mkdir ../out
	openssl x509 -in /tmp/client_cert.pem -outform der -out ../out/client_cert.der
	openssl rsa -in /tmp/client_key.pem -outform der -out ../out/client_key.der
.PHONY:	client_certs

clean_certs:
	rm -f ../out/client_cert.*
	rm -f ../out/client_key.*
.PHONY:	clean_certs

clean:	clean_open62541 clean_certs
.PHONY:	clean