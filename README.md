# open62541.project

## Create open62541 project
```bash
$ mkdir <app-dir>
$ cd <app-dir>
$ git clone git@gitlab.com/smic/open62541.project .project
$ mkdir src
# write source files in src directory
$ mkdir include
# write header files in include directory
```

## Project tree
```
<app-dir>
├── .project/       # open62541.project
│   ├── .git/
│   ├── .gitignore
│   ├── Makefile
│   ├── README.md
│   ├── bin/
│   ├── certs/      # certificate configuration
│   ├── cmake/      # build directory for external libraries
│   ├── include/    # header file directory for external libraries
│   ├── lib/        # static library directory for external libraries
│   ├── share/
│   └── src/        # mbedtls, open62541 source codes
├── include/        # header files for open62541 application
├── out/            # output files for open62541 application
└── src/            # source files for open62541 application
```

## Usage
```bash
$ make -C .project
Usage: make -C .project [rule...]

Rules:
  app                   Build open62541 application
  open62541             Build open62541 library
  client_certs          Generate certificate for open62541 client

  clean_app             Remove open62541 application
  clean_open62541       Remove open62541 library
  clean_certs           Remove certificate files
```